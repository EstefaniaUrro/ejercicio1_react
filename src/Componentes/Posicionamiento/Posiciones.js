import React, { Component } from "react";

export default class Posiciones extends Component {
  constructor() {
    super();
    this.state = { mouseX: 0, mouseY: 0 };
    this.gestionMouseMove = this.gestionMouseMove.bind(this);
    this.gestionClick = this.gestionClick.bind(this);
  }

  gestionMouseMove(e) {
    const { clientX, clientY } = e;
    this.setState({ mouseX: clientX, mouseY: clientY });
  }
  gestionClick(e) {
    alert(this.state.mouseX + this.state.mouseY);
  }
  render() {
    return (
      <div
        onMouseMove={this.gestionMouseMove}
        onClick={this.gestionClick}
        style={{ border: "1px solid black", marginTop: 10, padding: 10 }}
      >
        <p>
          {this.state.mouseX}, {this.state.mouseY}
        </p>
      </div>
    );
  }
}
