import React, { Component } from "react";

export default class Formulario2 extends Component {
    constructor(props) {
        super(props);
        this.inputNombre = React.createRef();
    }

    gestionEnvio = (e) => {
        e.preventDefault();
        const nombre = document.getElementById("nombre").value;
        console.log(nombre);
    };

    render() {
        return (
            <div>
                <h4>Formulario</h4>
                <h5>{this.props.children}</h5>
                <form>
                    <div className="form-group">
                        <label htmlFor="nombre">Nombre</label>
                        <input
                            className="form-control"
                            id="nombre"
                            name="nombre"
                            placeholder="Introduce el nombre"
                        />
                        <button
                            type="submit"
                            className="btn- btn-primary"
                            onClick={this.gestionEnvio}>
                            Enviar
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}
