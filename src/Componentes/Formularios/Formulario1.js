import React, { Component } from "react";

export default class Formulario1 extends Component {
    constructor(props) {
        super(props);
        this.inputNombre = React.createRef();
        this.inputEmail = React.createRef();
        this.state = {
            name: "Estefania Urro",
            email: "urroestefania@gmail.com"
        };
    }

    gestionEnvio = (e) => {
        e.preventDefault();
        const name = this.inputNombre.current.value;
        const email = this.inputEmail.current.value;
        console.log({ name, email });
    };

    render() {
        const { name, email } = this.state;
        return (
            <div>
                <h4>Formulario 1</h4>
                <h5>{this.props.children}</h5>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Nombre</label>
                        <input
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="Introduce el nombre"
                            ref={this.inputNombre}
                            value={name}
                            onChange={(e) => this.setState({ name: e.target.value })}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Correo Electrónico</label>
                        <input
                            className="form-control"
                            id="email"
                            name="email"
                            placeholder="Introduce el Correo electrónico"
                            ref={this.inputEmail}
                            value={email}
                            onChange={(e) => this.setState({ email: e.target.value })}
                        />
                    </div>
                    <button
                        type="submit"
                        className="btn- btn-primary"
                        onClick={this.gestionEnvio}>
                        Enviar
                    </button>
                </form>
            </div>
        );
    }
}
