import React, { Component } from "react";

export default class Alarma extends Component {
    estado = this.props.isActive ? "Si" : "No";
    render() {
        return (
            <div>
                <p> Estado: {this.estado} </p>
                <p> {this.props.mensaje} </p>
                <p> El valor es {this.props.valor} </p>
                <p> Listas: {this.props.lista.join(", ")} </p>
                {/* <!--<p> Objetos: {this.props.objeto.nombre} </p>--> */}
            </div>
        );
    }
}
Alarma.defaultProps = {
    valor: 0,
};
