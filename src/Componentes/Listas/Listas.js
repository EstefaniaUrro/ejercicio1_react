import React, { Component } from 'react';

class LineaInf extends Component {
    render() {
        return (
            <p> Hay {this.props.element} plazas a las {this.props.horaE}h. </p>
        )
    }
}

export default class Listas extends Component {

    render() {
        const horas = ['7:00', '8:10', '9:30', '11:10'];
        const numeros = [3, 6, 9, 2];
        let valor = [];
        numeros.forEach((element, index) => {
            valor.push(< LineaInf key={index}
                element={element}
                horaE={horas[index]}
            />)
        })
        return (
            <div>
                <h1> Listas </h1>
                <p>
                    Plazas: {" "}
                    {numeros.map(num => num * 2).join(', ')}
                </p>
                <p> Horas: {" "}
                    {horas.map((h, i) => {
                        return <span key={i} > {h} </span>
                    })
                    }
                </p>
                <h3> Disponibilidad </h3> {valor}
            </div>
        )
    }
}