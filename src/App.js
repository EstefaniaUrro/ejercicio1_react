import React from "react";
import "./App.css";
import Alarma from "./Componentes/Alarma/Alarma";
import Posiciones from "./Componentes/Posicionamiento/Posiciones";
import Formulario1 from "./Componentes/Formularios/Formulario1";
import Formulario2 from "./Componentes/Formularios/Forrmulario2";
import Interruptor from "./Componentes/Interruptor/Interruptor";
import Listas from "./Componentes/Listas/Listas";

function Hola(props) {
    return (
        <div>
            <Interruptor />
            <hr style={{ backgroundColor: "green", height: "5" }} />
            <h1>Función para uso de props</h1>
            <p> Y seguimos con los holas, {props.author} </p>
        </div>
    );
}

function App() {
    return (
        <div className="App">
            <h1>Componente de posiciones</h1>
            <Posiciones />
            <hr style={{ backgroundColor: "green", height: "5" }} />
            <h1>Componente de formularios</h1>
            <Formulario1>
                <div>Hola</div>
                Este formulario se emplea para los datos de los conectados
            </Formulario1>
            <Formulario2 />
            <hr style={{ backgroundColor: "green", height: "5" }} />
            <h1>Función para llamar componente Interruptor</h1>
            <Hola author="Estefania Urro" />
            <hr style={{ backgroundColor: "green", height: "5" }} />
            <h1>Componente de alarma</h1>
            <Alarma
                isActive
                mensaje="Esto funciona" /*valor = {3}*/
                lista={["casa", "perro", "camion"]}
            /*objeto = {{ nombre: "Estefania", apellido: "Urro" }}*/
            />
            <hr style={{ backgroundColor: "green", height: "5" }} />
            <h1>Componente de listas</h1>
            <Listas />
            <hr style={{ backgroundColor: "green", height: "5" }} />
            <h1>Botón para alert</h1>
            <button onClick={() => alert("Hola, que tal")}> Saludo </button>
        </div>
    );
}

export default App;
